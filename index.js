/* 
    Instructions s32 Activity:
    1. In the S32 folder, create an a1 folder and an index.js file inside of it.
    2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
    a. If the url is http://localhost:4000/, send a response Welcome to Booking System
    b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
    c. If the url is http://localhost:4000/courses, send a response Here’s our courses available
    d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
    e. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
    f. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
    3. Test all the endpoints in Postman.
    4. Create a git repository named S32.
    5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
    6. Add the link in Boodle.


*/

const http = require("http");
const port = 4000;

const server = http.createServer((req, res) => {
    if (req.url == "/" && req.method == "GET"){
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(`Welcome to Booking System`);
    }
    
    else if (req.url == "/profile" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(`Welcome to your profile!`);
        res.end();
    }
    
    else if (req.url == "/courses" && req.method == "GET"){
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(`Here’s our courses available`);
        res.end();
    }

    else if (req.url == "/addcourse" && req.method == "POST"){
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(`Add a course to our resources`);
        res.end();
    }

    else if (req.url == "/updatecourse" && req.method == "PUT"){
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(`Update a course to our resources`);
        res.end();
    }
    else if (req.url == "/archivecourses" && req.method == "DELETE"){
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(`Archive courses to our resources`);
        res.end();
    }

    else {
        res.writeHead(404, { "Content-Type": "application/json" });
        res.end(`I'm sorry the page you are looking for cannot be found.`);
    }

});

server.listen(port);

console.log(`Server is running at localhost ${port}`);